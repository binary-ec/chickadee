;;; Chickadee Game Toolkit
;;; Copyright © 2016 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; Development environment for GNU Guix.
;;
;; To setup the development environment, run the following:
;;
;;    guix shell
;;    ./bootstrap && ./configure;
;;
;; To build the development snapshot, run:
;;
;;    guix build -f guix.scm
;;
;; To install the development snapshot, run:
;;
;;    guix install -f guix.scm
;;
;;; Code:

(use-modules (ice-9 match)
             (srfi srfi-1)
             (guix packages)
             ((guix licenses) #:prefix license:)
             (guix download)
             (guix git)
             (guix git-download)
             (guix build-system gnu)
             (guix utils)
             (gnu packages)
             (gnu packages audio)
             (gnu packages autotools)
             (gnu packages fontutils)
             (gnu packages image)
             (gnu packages pkg-config)
             (gnu packages readline)
             (gnu packages texinfo)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages gl)
             (gnu packages sdl)
             (gnu packages maths)
             (gnu packages mp3)
             (gnu packages xiph))

(define target-guile guile-next)

(define guile-sdl2
  (let ((commit "e9a7f5e748719ce5b6ccd08ff91861b578034ea6"))
    (package
      (name "guile-sdl2")
      (version (string-append "0.7.0-1." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.dthompson.us/guile-sdl2.git")
                      (commit commit)))
                (sha256
                 (base32
                  "0ay7mcar8zs0j5rihwlzi0l46vgg9i93piip4v8a3dzwjx3myr7v"))))
      (build-system gnu-build-system)
      (arguments
       '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
      (native-inputs
       (list autoconf automake pkg-config texinfo))
      (inputs
       (list target-guile sdl2))
      (synopsis "Guile bindings for SDL2")
      (description "Guile-sdl2 provides pure Guile Scheme bindings to the
SDL2 C shared library via the foreign function interface.")
      (home-page "https://git.dthompson.us/guile-sdl2.git")
      (license license:lgpl3+))))

(package
  (name "chickadee")
  (version "0.10.0-git")
  (source (git-checkout (url (dirname (current-filename)))))
  (build-system gnu-build-system)
  (arguments
   '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
  (native-inputs
   (list autoconf automake guile-syntax-highlight pkg-config texinfo))
  (inputs
   (list freetype
         target-guile
         libjpeg-turbo
         libpng
         libvorbis
         mpg123
         openal
         readline))
  (propagated-inputs
   (list guile3.0-opengl guile-sdl2))
  (synopsis "Game development toolkit for Guile Scheme")
  (description "Chickadee is a game development toolkit for Guile
Scheme.  It contains all of the basic components needed to develop
2D/3D video games.")
  (home-page "https://dthompson.us/projects/chickadee.html")
  (license license:gpl3+))
