(use-modules (chickadee)
             (chickadee graphics text)
             (chickadee math vector)
             (chickadee scripting))

(define start-time 0.0)
(define avg-frame-time 16.0)
(define stats-text "")
(define stats-position (vec2 4.0 704.0))
(define position (vec2 160.0 240.0))
(define text "The quick brown fox jumps over the lazy dog.\nFive hexing wizard bots jump quickly.")

(define (stats-message)
  (format #f "fps: ~1,2f"
          (/ 1.0 avg-frame-time)))

(define (load)
  (script
   (forever
    (sleep 60)
    (set! stats-text (stats-message)))))

(define (draw alpha)
  (draw-text text position)
  (draw-text stats-text stats-position)
  (let ((current-time (elapsed-time)))
    (set! avg-frame-time
          (+ (* (- current-time start-time) 0.1)
             (* avg-frame-time 0.9)))
    (set! start-time current-time)))

(define (update dt)
  (update-agenda 1))

(define (key-press key modifiers repeat?)
  (when (eq? key 'q)
    (abort-game)))

(run-game #:draw draw
          #:key-press key-press
          #:load load
          #:update update
          #:window-title "text rendering")
