(use-modules (chickadee)
             (chickadee math matrix)
             (chickadee math rect)
             (chickadee math vector)
             (chickadee graphics color)
             (chickadee graphics sprite)
             (chickadee graphics text)
             (chickadee graphics texture)
             (chickadee scripting)
             (ice-9 format)
             (ice-9 match)
             (srfi srfi-1)
             (statprof))

(define texture #f)
(define batch #f)
(define start-time 0.0)
(define avg-frame-time 16)
(define num-sprites 5000)
(define sprites
  (list-tabulate num-sprites
                 (lambda (n)
                   (list (rect (* (random:uniform) 640.0)
                               (* (random:uniform) 480.0)
                               16.0 16.0)
                         (vec2 (* (- (random:uniform) 0.5) 0.5)
                               (* (- (random:uniform) 0.5) 0.5))))))
(define matrix (make-identity-matrix4))

(define (stats-message)
  (format #f "sprites: ~d   fps: ~1,2f"
          num-sprites
          (/ 1.0 avg-frame-time)))

(define stats-text (stats-message))

(define (load)
  (set! *random-state* (random-state-from-platform))
  (set! texture (load-image "images/shot.png"))
  (set! batch (make-sprite-batch texture #:capacity num-sprites))
  (script
   (forever
    (sleep 60)
    (set! stats-text (pk 'stats (stats-message))))))

(define stats-text-pos (vec2 4.0 464.0))
(define (draw alpha)
  (sprite-batch-clear! batch)
  (for-each (match-lambda
             ((r v)
              (set-rect-x! r (+ (rect-x r) (vec2-x v)))
              (set-rect-y! r (+ (rect-y r) (vec2-y v)))
              (sprite-batch-add* batch r matrix)))
            sprites)
  (draw-sprite-batch batch)
  (draw-text stats-text stats-text-pos #:color black)
  (let ((current-time (elapsed-time)))
    (set! avg-frame-time
          (+ (* (- current-time start-time) 0.1)
             (* avg-frame-time 0.9)))
    (set! start-time current-time)))

(define (update dt)
  (update-agenda 1))

(run-game #:load load #:draw draw #:update update
          #:window-title "sprite batch stress test")
