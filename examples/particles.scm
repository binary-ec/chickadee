(use-modules (chickadee)
             (chickadee math rect)
             (chickadee math vector)
             (chickadee graphics color)
             (chickadee graphics particles)
             (chickadee graphics sprite)
             (chickadee graphics text)
             (chickadee graphics texture)
             (chickadee scripting)
             (ice-9 format))

(define window-width 800)
(define window-height 600)
(define particles #f)
(define emitter #f)
(define particle-texture #f)
(define sprite-texture #f)
(define start-time 0.0)
(define avg-frame-time 0)
(define stats-text "")

(define (center-x w)
  (- (/ window-width 2.0) (/ w 2.0)))

(define (center-y h)
  (- (/ window-height 2.0) (/ h 2.0)))

(define (load)
  (set! *random-state* (random-state-from-platform))
  (set! particle-texture (load-image "images/explosion.png"))
  (set! sprite-texture (load-image "images/chickadee.png"))
  (set! particles (make-particles 2000
                                  #:texture particle-texture
                                  #:end-color (make-color 1.0 1.0 1.0 0.8)
                                  #:end-color (make-color 1.0 1.0 1.0 0.0)
                                  #:speed-range (vec2 1.0 5.0)
                                  ;;#:acceleration-range (vec2 -0.1 -0.2)
                                  #:lifetime 40
                                  #:animation-columns 12
                                  #:sort 'young))
  (set! emitter (make-particle-emitter (make-rect (center-x 0.0)
                                                  (center-y 0.0)
                                                  0.0 0.0)
                                       8))
  (add-particle-emitter particles emitter)
  (script
   (forever
    (sleep 60)
    (set! stats-text (format #f "particles: ~d   fps: ~1,2f"
                             (particles-size particles)
                             (/ 1.0 avg-frame-time))))))

(define stats-text-pos (vec2 4.0 (- window-height 16.0)))
(define sprite-position (vec2 (center-x 128.0) (center-y 128.0)))
(define (draw alpha)
  (draw-particles particles)
  (draw-sprite sprite-texture sprite-position)
  (draw-text stats-text stats-text-pos)
  (let ((current-time (elapsed-time)))
    (set! avg-frame-time
          (+ (* (- current-time start-time) 0.1)
             (* avg-frame-time 0.9)))
    (set! start-time current-time)))

(define (update dt)
  (update-agenda 1)
  (update-particles particles))

(define (mouse-move x y dx dy buttons)
  (let ((area (particle-emitter-spawn-area emitter)))
    (set-rect-x! area x)
    (set-rect-y! area y)
    (set-vec2! sprite-position (- x 64.0) (- y 64.0))))

(run-game #:load load
          #:draw draw
          #:update update
          #:mouse-move mouse-move
          #:window-width window-width
          #:window-height window-height)
