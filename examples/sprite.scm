(use-modules (chickadee)
             (chickadee math vector)
             (chickadee graphics sprite)
             (chickadee graphics texture))

(define sprite #f)

(define (load)
  (set! sprite (load-image "images/chickadee.png")))

(define (draw alpha)
  (draw-sprite sprite (vec2 256.0 176.0)))

(run-game #:load load #:draw draw)
