## Chickadee Game Toolkit
## Copyright © 2016 David Thompson <dthompson2@worcester.edu>
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##    http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.

GOBJECTS = $(SOURCES:%.scm=%.go)

nobase_mod_DATA = $(SOURCES) $(NOCOMP_SOURCES)
nobase_go_DATA = $(GOBJECTS)

# Make sure source files are installed first, so that the mtime of
# installed compiled files is greater than that of installed source
# files.  See
# <http://lists.gnu.org/archive/html/guile-devel/2010-07/msg00125.html>
# for details.
guile_install_go_files = install-nobase_goDATA
$(guile_install_go_files): install-nobase_modDATA

CLEANFILES = $(GOBJECTS)
EXTRA_DIST = $(SOURCES) $(NOCOMP_SOURCES)
GUILE_WARNINGS = -Wunbound-variable -Warity-mismatch -Wformat
SUFFIXES = .scm .go
.scm.go:
	$(AM_V_GEN)$(top_builddir)/pre-inst-env $(GUILE_TOOLS) compile $(GUILE_WARNINGS) -o "$@" "$<"

moddir=$(prefix)/share/guile/site/$(GUILE_EFFECTIVE_VERSION)
godir=$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache

SOURCES = 					\
  chickadee/config.scm				\
  chickadee/utils.scm				\
  chickadee/game-loop.scm			\
  chickadee/json.scm				\
  chickadee/base64.scm				\
  chickadee/freetype.scm			\
  chickadee/readline.scm			\
  chickadee/async-repl.scm			\
  chickadee/data/heap.scm			\
  chickadee/data/array-list.scm			\
  chickadee/data/queue.scm			\
  chickadee/data/bytestruct.scm			\
  chickadee/data/quadtree.scm			\
  chickadee/data/grid.scm			\
  chickadee/data/path-finding.scm		\
  chickadee/math.scm				\
  chickadee/math/vector.scm			\
  chickadee/math/bezier.scm			\
  chickadee/math/matrix.scm			\
  chickadee/math/quaternion.scm			\
  chickadee/math/rect.scm			\
  chickadee/math/easings.scm			\
  chickadee/audio/mpg123.scm			\
  chickadee/audio/openal.scm			\
  chickadee/audio/vorbis.scm			\
  chickadee/audio/wav.scm			\
  chickadee/audio.scm				\
  chickadee/image.scm				\
  chickadee/image/jpeg.scm			\
  chickadee/image/png.scm			\
  chickadee/graphics/gl.scm			\
  chickadee/graphics/engine.scm			\
  chickadee/graphics/color.scm			\
  chickadee/graphics/blend.scm			\
  chickadee/graphics/polygon.scm		\
  chickadee/graphics/depth.scm			\
  chickadee/graphics/stencil.scm		\
  chickadee/graphics/multisample.scm		\
  chickadee/graphics/buffer.scm			\
  chickadee/graphics/pixbuf.scm			\
  chickadee/graphics/texture.scm		\
  chickadee/graphics/shader.scm			\
  chickadee/graphics/viewport.scm		\
  chickadee/graphics/framebuffer.scm		\
  chickadee/graphics/sprite.scm			\
  chickadee/graphics/9-patch.scm		\
  chickadee/graphics/text.scm			\
  chickadee/graphics/tile-map.scm		\
  chickadee/graphics/particles.scm		\
  chickadee/graphics/skybox.scm			\
  chickadee/graphics/light.scm			\
  chickadee/graphics/mesh.scm			\
  chickadee/graphics/phong.scm			\
  chickadee/graphics/pbr.scm			\
  chickadee/graphics/model.scm			\
  chickadee/graphics/path.scm			\
  chickadee/scripting/agenda.scm		\
  chickadee/scripting/script.scm		\
  chickadee/scripting/channel.scm		\
  chickadee/scripting.scm			\
  chickadee.scm					\
  chickadee/cli.scm				\
  chickadee/cli/play.scm			\
  chickadee/cli/bundle.scm

TESTS =						\
  tests/bytestruct.scm				\
  tests/base64.scm				\
  tests/vector.scm				\
  tests/rect.scm				\
  tests/matrix.scm				\
  tests/array-list.scm				\
  tests/heap.scm				\
  tests/quadtree.scm				\
  tests/queue.scm

TEST_EXTENSIONS = .scm
SCM_LOG_COMPILER = $(top_builddir)/test-env $(GUILE)
AM_SCM_LOG_FLAGS = --no-auto-compile -L "$(top_srcdir)"

EXTRA_DIST += 						\
  COPYING						\
  guix.scm						\
  run-example						\
  examples/audio.scm					\
  examples/sprite.scm					\
  examples/text.scm					\
  examples/9-patch.scm					\
  examples/particles.scm				\
  examples/tile-map.scm					\
  examples/grid.scm					\
  examples/game-controller.scm				\
  examples/sprite-batch.scm				\
  examples/model.scm					\
  examples/path.scm					\
  examples/quadtree.scm					\
  examples/audio/AUTHORS				\
  examples/audio/explosion.wav				\
  examples/audio/spooky-dungeon.ogg			\
  examples/audio/the-forgotten-land.mp3			\
  examples/audio/venus.wav				\
  examples/images/AUTHORS				\
  examples/images/chickadee.png				\
  examples/images/controller-buttons.png		\
  examples/images/dialog-box.png			\
  examples/images/explosion.png				\
  examples/images/shot.png				\
  examples/images/serene-village.png			\
  examples/maps/example.tmx				\
  examples/maps/serene-village.tsx			\
  examples/models/Suzanne/Suzanne.bin			\
  examples/models/Suzanne/Suzanne.gltf			\
  examples/models/Suzanne/Suzanne_BaseColor.png		\
  examples/models/Suzanne/Suzanne_MetallicRoughness.png	\
  tests/utils.scm					\
  $(TESTS)

CLEANFILES +=						\
  $(TESTS:tests/%.scm=%.log)

bin_SCRIPTS =						\
  scripts/chickadee

dist_pkgdata_DATA = 				\
  data/AUTHORS					\
  data/gamecontrollerdb.txt

fontsdir = $(pkgdatadir)/fonts
dist_fonts_DATA =				\
  data/fonts/Inconsolata-Regular.otf

shadersdir = $(pkgdatadir)/shaders
dist_shaders_DATA =				\
  data/shaders/path-fill-frag.glsl		\
  data/shaders/path-fill-vert.glsl		\
  data/shaders/path-stroke-frag.glsl		\
  data/shaders/path-stroke-vert.glsl		\
  data/shaders/pbr-vert.glsl			\
  data/shaders/pbr-frag.glsl			\
  data/shaders/phong-vert.glsl			\
  data/shaders/phong-frag.glsl			\
  data/shaders/skybox-frag.glsl			\
  data/shaders/skybox-vert.glsl

info_TEXINFOS = doc/chickadee.texi
doc_chickadee_TEXINFOS =			\
  doc/apache-2.0.texi				\
  doc/api.texi

# Don't build dvi docs.
dvi:

html-local:
	$(GUILE) --no-auto-compile doc/build-html.scm

publish: distcheck
	gpg --sign --detach-sign --armor --yes chickadee-$(VERSION).tar.gz && \
        scp chickadee-$(VERSION).tar.gz chickadee-$(VERSION).tar.gz.asc \
	    publish@dthompson.us:/var/www/files/chickadee/
