// -*- mode: c -*-

#ifdef GLSL330
layout (location = 0) in vec2 position;
layout (location = 1) in vec2 tex;
layout (location = 2) in float strokeLength;
#elif defined(GLSL130)
in vec2 position;
in vec2 tex;
in float strokeLength;
#elif defined(GLSL120)
attribute vec2 position;
attribute vec2 tex;
attribute float strokeLength;
#endif

#ifdef GLSL120
varying vec2 fragTex;
varying float fragStrokeLength;
#else
out vec2 fragTex;
out float fragStrokeLength;
#endif

uniform mat4 mvp;
uniform vec4 color;
uniform int mode;
uniform int strokeClosed;

void main(void) {
  // Short-circuit because the fragments will just be discarded anyway.
  if (color.a <= 0.0) {
    gl_Position = vec4(0.0, 0.0, 0.0, 1.0);
  } else {
    fragStrokeLength = strokeLength;
    fragTex = tex;
    gl_Position = mvp * vec4(position.xy, 0.0, 1.0);
  }
}
