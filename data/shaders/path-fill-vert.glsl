// -*- mode: c -*-

#ifdef GLSL330
layout (location = 0) in vec2 position;
#elif defined(GLSL130)
in vec2 position;
#elif defined(GLSL120)
attribute vec2 position;
#endif

#ifdef GLSL120
varying vec2 fragPosition;
#else
out vec2 fragPosition;
#endif

uniform mat4 mvp;
uniform vec4 color;
uniform mat3 gradientMatrix;

void main(void) {
  // Short-circuit because the fragments will just be discarded
  // anyway.
  if (color.a <= 0.0) {
    gl_Position = vec4(0.0, 0.0, 0.0, 1.0);
  } else {
    fragPosition = (gradientMatrix * vec3(position, 1.0)).xy;
    gl_Position = mvp * vec4(position, 0.0, 1.0);
  }
}
