// -*- mode: c -*-

#ifdef GLSL330
out vec4 fragColor;
#else
#define fragColor gl_FragColor
#endif

#ifdef GLSL120
varying vec2 fragTex;
varying float fragStrokeLength;
#else
in vec2 fragTex;
in float fragStrokeLength;
#endif

uniform vec4 color;
uniform float feather;
uniform int strokeClosed;
uniform float strokeWidth;
uniform int strokeCap;
uniform int strokeMiterStyle;
uniform float strokeMiterLimit;

float infinity = 1.0 / 0.0;

void main(void) {
  if (color.a <= 0.0) {
    discard;
  }

  float hw = strokeWidth / 2.0;
  float u = fragTex.x;
  float v = fragTex.y;
  float dx;
  float dy;
  float d;

  // Stroke caps.
  if (u < 0 || u > fragStrokeLength) {
    if (u < 0) {
      dx = abs(u);
    } else {
      dx = u - fragStrokeLength;
    }
    dy = abs(v);

    if (strokeCap == 0) { // none
      d = infinity;
    } else if (strokeCap == 1) { // butt
      d = max(dx + hw - 2 * feather, dy);
    } else if (strokeCap == 2) { // square
      d = max(dx, dy);
    } else if (strokeCap == 3) { // round
      d = sqrt(dx * dx + dy * dy);
    } else if (strokeCap == 4) { // triangle out
      d = dx + dy;
    } else if (strokeCap == 5) { // triangle in
      d = max(dy, hw - feather + dx - dy);
    }
    // Stroke inner/join
  } else {
    d = abs(v);
  }

  if(d <= hw) {
    fragColor = color;
  } else {
    vec4 c = vec4(color.rgb, color.a * (1.0 - ((d - hw) / feather)));

    if (c.a <= 0.0) {
      discard;
    }

    fragColor = c;
  }
}
