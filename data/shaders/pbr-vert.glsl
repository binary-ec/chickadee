// -*- mode: c -*-

#ifdef GLSL330
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texcoord0;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec2 texcoord1;
layout (location = 5) in vec4 color0;
layout (location = 6) in vec4 joint;
layout (location = 7) in vec4 weight;
#elif defined(GLSL130)
in vec3 position;
in vec2 texcoord0;
in vec3 normal;
in vec3 tangent;
in vec2 texcoord1;
in vec4 color0;
in vec4 joint;
#elif defined(GLSL120)
attribute vec3 position;
attribute vec2 texcoord0;
attribute vec3 normal;
attribute vec3 tangent;
attribute vec2 texcoord1;
attribute vec4 color0;
attribute vec4 weight;
#endif

#ifdef GLSL120
varying vec3 fragWorldPos;
varying vec3 fragNormal;
varying vec3 fragTangent;
varying vec2 fragTexcoord0;
varying vec2 fragTexcoord1;
varying vec4 fragColor0;
#else
out vec3 fragWorldPos;
out vec3 fragNormal;
out vec3 fragTangent;
out vec2 fragTexcoord0;
out vec2 fragTexcoord1;
out vec4 fragColor0;
#endif

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main(void) {
  fragWorldPos = vec3(model * vec4(position, 1.0));
  fragNormal = mat3(model) * normal;
  fragTangent = mat3(model) * tangent;
  fragTexcoord0 = texcoord0;
  fragTexcoord1 = texcoord1;
  fragColor0 = color0;
  gl_Position = projection * view * vec4(fragWorldPos, 1.0);
}
