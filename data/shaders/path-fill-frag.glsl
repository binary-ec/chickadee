// -*- mode: c -*-

#ifdef GLSL330
out vec4 fragColor;
#else
#define fragColor gl_FragColor
#endif

#ifdef GLSL120
varying vec2 fragPosition;
#else
in vec2 fragPosition;
#endif

uniform int mode;
uniform vec4 color;
uniform vec4 endColor;
uniform vec2 gradientRange;
uniform float radialGradientRatio;

vec4 gradientMix(float x) {
  float start = gradientRange.x;
  float end = gradientRange.y;
  float t = clamp((x - start) / (end - start), 0.0, 1.0);
  return mix(color, endColor, t);
}

void main(void) {
  if (color.a <= 0.0) {
    discard;
  }

  if(mode == 0) { // solid color
    fragColor = color;
  } else if(mode == 1) { // linear gradient
    fragColor = gradientMix(fragPosition.x);
  } else if(mode == 2) { // radial gradient
    vec2 p = fragPosition * vec2(1.0, radialGradientRatio);
    fragColor = gradientMix(length(p));
  }
}
