// -*- mode: c -*-

#ifdef GLSL330
layout (location = 0) in vec3 position;
#elif defined(GLSL130)
in vec3 position;
#elif defined(GLSL120)
attribute vec3 position;
#endif

#ifdef GLSL120
varying vec3 fragDir;
#else
out vec3 fragDir;
#endif

uniform mat4 view;
uniform mat4 projection;

void main() {
  // Remove the translation part of the view matrix.
  mat4 viewDir = mat4(mat3(view));
  fragDir = position;
  gl_Position = projection * viewDir * vec4(position, 1.0);
}
