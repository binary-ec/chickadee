;;; Chickadee Game Toolkit
;;; Copyright © 2023 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (tests base64)
  #:use-module (chickadee base64)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-64)
  #:use-module (tests utils))

(with-tests "base64"
  (test-group "base64-decode"
    (test-equal "encoded text no padding"
      "hello!"
      (utf8->string (base64-decode "aGVsbG8h")))
    (test-equal "encoded text with one byte of padding"
      "hello"
      (utf8->string (base64-decode "aGVsbG8=")))
    (test-equal "encoded text with two bytes of padding"
      "what's up?"
      (utf8->string (base64-decode "d2hhdCdzIHVwPw==")))))
