;;; Chickadee Game Toolkit
;;; Copyright © 2021 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (tests queue)
  #:use-module (tests utils)
  #:use-module (srfi srfi-64)
  #:use-module (chickadee data queue))

(with-tests "queue"
  (let ((q (make-queue)))
    (enqueue! q 'a)
    (enqueue! q 'b)
    (enqueue! q 'c)
    (test-equal "enqueue!" (queue-length q) 3)
    (test-equal "dequeue!" (dequeue! q) 'a)
    (queue-clear! q)
    (test-assert "queue-clear!" (queue-empty? q))))
