;;; Chickadee Game Toolkit
;;; Copyright © 2021 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (tests heap)
  #:use-module (tests utils)
  #:use-module (srfi srfi-64)
  #:use-module (chickadee data heap))

(with-tests "heap"
  (let ((h (make-heap)))
    (heap-insert! h 3)
    (heap-insert! h 1)
    (heap-insert! h 2)
    (test-equal "heap-insert!" (heap-size h) 3)
    (test-equal "heap-min!" (heap-min h) 1)
    (heap-remove! h)
    (test-equal "heap-remove!" (heap-min h) 2)
    (heap-clear! h)
    (test-assert "heap-clear!" (heap-empty? h))))
