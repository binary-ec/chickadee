;;; Chickadee Game Toolkit
;;; Copyright © 2022 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (tests matrix)
  #:use-module (tests utils)
  #:use-module (srfi srfi-64)
  #:use-module (chickadee math)
  #:use-module (chickadee math matrix)
  #:use-module (chickadee math quaternion)
  #:use-module (chickadee math vector))

(with-tests "matrix"
  (test-group "3x3 matrices"
    (test-assert "matrix3-copy"
      (let ((m (make-matrix3 0 1 2
                             3 4 5
                             6 7 8)))
        (matrix3= m (matrix3-copy m))))
    (test-assert "matrix3-copy!"
      (let ((src (make-matrix3 0 1 2
                               3 4 5
                               6 7 8))
            (dest (make-null-matrix3)))
        (matrix3-copy! src dest)
        (matrix3= src dest))))
  (test-group "4x4 matrices"
    (test-assert "matrix4-copy"
      (let ((m (make-matrix4 0 1 2 3
                             4 5 6 7
                             8 9 0 1
                             2 3 4 5)))
        (matrix4= m (matrix4-copy m))))
    (test-assert "matrix4-copy!"
      (let ((src (make-matrix4 0 1 2 3
                               4 5 6 7
                               8 9 0 1
                               2 3 4 5))
            (dest (make-null-matrix4)))
        (matrix4-copy! src dest)
        (matrix4= src dest)))
    (test-group "multiplication"
      (test-assert "identity"
        (let ((m (make-matrix4 0 1 2 3
                               4 5 6 7
                               8 9 0 1
                               2 3 4 5)))
          (matrix4= m (matrix4* (make-identity-matrix4) m))))
      (test-assert "inverse"
        ;; Using whole numbers to avoid floating point precision
        ;; issues where the resulting identity matrix has values very
        ;; close to 1.0 (like 0.9999999403953552) but not exactly,
        ;; causing the test to fail.
        (let ((m (matrix4-scale (vec3 2 3 4))))
          (matrix4= (make-identity-matrix4)
                    (matrix4* m (matrix4-inverse m))))))
    (test-group "transformation"
      (test-assert "identity (vec2)"
        (let ((v (vec2 1 2)))
          (vec2= v (matrix4-transform-vec2 (make-identity-matrix4) v))))
      (test-assert "identity (vec3)"
        (let ((v (vec3 1 2 3)))
          (vec3= v (matrix4-transform-vec3 (make-identity-matrix4) v))))
      (test-assert "translate"
        (vec3= (vec3 5 7 9)
               (matrix4-transform-vec3 (matrix4-translate (vec3 4 5 6))
                                       (vec3 1 2 3))))
      (test-assert "scale"
        (vec3= (vec3 6 12 20)
               (matrix4-transform-vec3 (matrix4-scale (vec3 3 4 5))
                                       (vec3 2 3 4))))
      (test-assert "rotate"
        (vec3= (vec3 0 1 0)
               (matrix4-transform-vec3 (matrix4-rotate
                                        (rotation->quaternion (vec3 0 0 -1)
                                                              (/ pi 2)))
                                       (vec3 1 0 0)))))))
