;;; Chickadee Game Toolkit
;;; Copyright © 2021 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (tests array-list)
  #:use-module (tests utils)
  #:use-module (srfi srfi-64)
  #:use-module (chickadee data array-list))

(with-tests "array-list"
  ;; Using an initial capacity of 2 to force an expansion when adding
  ;; the third element.
  (let ((a (make-array-list 2)))
    (array-list-push! a 'a)
    (array-list-push! a 'b)
    (array-list-push! a 'c)
    (test-equal "array-list-fold"
      (array-list-fold (lambda (i item prev)
                         (cons item prev))
                       '() a)
      '(c b a))
    (test-equal "array-list-push!" (array-list-size a) 3)
    (test-equal "array-list-pop!" (array-list-pop! a) 'c)
    (array-list-set! a 1 'd)
    (test-equal "array-list-set!" (array-list-ref a 1) 'd)
    (array-list-delete! a 'a)
    (test-equal "array-list-delete!" (array-list-ref a 0) 'd)
    (array-list-clear! a)
    (test-assert "array-list-clear!" (array-list-empty? a))))
