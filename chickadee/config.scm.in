;;; Chickadee Game Toolkit
;;; Copyright © 2016 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; Build time configuration.
;;
;;; Code:

(define-module (chickadee config)
  #:use-module (ice-9 match)
  #:use-module (system foreign)
  #:export (dynamic-link*
            %datadir
            %chickadee-version
            %libpng
            %libpng-version
            %libturbojpeg
            %libopenal
            %libvorbisfile
            %libmpg123
            %libfreetype
            %libreadline
            scope-datadir))

;; Try to link against multiple library possibilities, such as the
;; absolute file name discovered by ./configure or by searching the
;; library load path as a fallback method.  Useful when distributing
;; relocatable builds.
(define (dynamic-link* names)
  (let loop ((names* names))
    (match names*
      (()
       (error "could not find library" names))
      ((name . rest)
       (or (false-if-exception (dynamic-link name))
           (loop rest))))))

(define %datadir
  (or (getenv "CHICKADEE_DATADIR") "@chickadee_datadir@/chickadee"))

(define %chickadee-version "@PACKAGE_VERSION@")

;; The version of libpng that chickadee was built against.  Used for
;; initializing libpng.
(define %libpng-version "@PNG_VERSION@")

;; When LD_LIBRARY_PATH is set, we *don't* want to use the absolute
;; file name of the library that pkgconfig found.  Instead, we want to
;; use the library name itself so the search path is used.
(define-syntax-rule (define-library-name name
                      (absolute ...)
                      (relative ...))
  (define name
    (cond
     ((getenv "LD_LIBRARY_PATH")
      '(relative ...))
     (else
      '(absolute ...)))))

(define-library-name %libpng
  ("@PNG_LIBDIR@/libpng")
  ("libpng"))
(define-library-name %libturbojpeg
  ("@TURBOJPEG_LIBDIR@/libturbojpeg")
  ("libturbojpeg"))
(define-library-name %libopenal
  ("@OPENAL_LIBDIR@/libopenal")
  ("libopenal"))
(define-library-name %libvorbisfile
  ("@VORBIS_LIBDIR@/libvorbisfile")
  ("libvorbisfile"))
(define-library-name %libmpg123
  ("@MPG123_LIBDIR@/libmpg123")
  ("libmpg123"))
(define-library-name %libfreetype
  ("@FREETYPE_LIBDIR@/libfreetype")
  ("libfreetype"))
(define-library-name %libreadline
  ("@READLINE_LIBDIR@/libreadline")
  ("libreadline"))

(define (scope-datadir file)
  "Append the Chickadee data directory to FILE."
  (string-append %datadir "/" file))
