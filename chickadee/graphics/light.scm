;;; Chickadee Game Toolkit
;;; Copyright © 2021 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; 3D lights.
;;
;;; Code:

(define-module (chickadee graphics light)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics shader)
  #:use-module (chickadee math)
  #:use-module (chickadee math vector)
  #:export (%max-lights
            make-point-light
            make-directional-light
            make-spot-light
            %disabled-light
            light?
            point-light?
            directional-light?
            spot-light?
            light-enabled?
            light-type
            light-position
            light-direction
            light-color
            light-intensity
            light-cut-off
            set-light-position!
            set-light-direction!
            set-light-color!
            set-light-intensity!
            set-light-cut-off!))

;; Maximum number of lights supported by our shaders.
(define %max-lights 4)

(define-shader-type <light>
  make-light
  light?
  (bool enabled light-enabled?)
  (int type %light-type)
  (float-vec3 position light-position set-light-position!)
  (float-vec3 direction light-direction set-light-direction!)
  (float-vec4 color light-color set-light-color!)
  (float intensity light-intensity set-light-intensity!)
  (float cut-off light-cut-off %set-light-cut-off!))

(define %disabled-light (make-light #:enabled #f))

(define (light-type light)
  (let ((t (light-type light)))
    (cond
     ((= t 0) 'point)
     ((= t 1) 'directional)
     ((= t 2) 'spot))))

(define (point-light? light)
  (eq? (light-type light) 'point))

(define (directional-light? light)
  (eq? (light-type light) 'directional))

(define (spot-light? light)
  (eq? (light-type light) 'spot))

(define (set-light-cut-off! light cut-off)
  (%set-light-cut-off! light (cos cut-off)))

(define* (make-point-light #:key (position (vec3 0.0 0.0 0.0))
                           (color black) (intensity 1.0))
  (make-light #:enabled #t
              #:type 0
              #:position position
              #:color color
              #:intensity intensity))

(define* (make-directional-light #:key (direction (vec3 0.0 -1.0 0.0))
                                 (color black) (intensity 1.0))
  (make-light #:enabled #t
              #:type 1
              #:direction direction
              #:color color
              #:intensity intensity))

(define* (make-spot-light #:key (position (vec3 0.0 0.0 0.0))
                          (direction (vec3 0.0 0.0 -1.0))
                          (color black)
                          (cut-off (/ pi 4.0))
                          (intensity 1.0))
  (make-light #:enabled #t
              #:type 2
              #:position position
              #:direction direction
              #:color color
              #:cut-off (cos cut-off)
              #:intensity intensity))
