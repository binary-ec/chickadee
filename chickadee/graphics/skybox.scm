;;; Chickadee Game Toolkit
;;; Copyright © 2021 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; 3D Skybox
;;
;;; Code:

(define-module (chickadee graphics skybox)
  #:use-module (chickadee config)
  #:use-module (chickadee graphics buffer)
  #:use-module (chickadee graphics blend)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics depth)
  #:use-module (chickadee graphics engine)
  #:use-module (chickadee graphics pixbuf)
  #:use-module (chickadee graphics polygon)
  #:use-module (chickadee graphics shader)
  #:use-module (chickadee graphics texture)
  #:use-module (srfi srfi-9)
  #:export (make-skybox
            skybox?
            skybox-cube-map
            skybox-mesh
            default-skybox
            draw-skybox))

(define-record-type <skybox>
  (%make-skybox cube-map vertex-array)
  skybox?
  (cube-map skybox-cube-map)
  (vertex-array skybox-vertex-array))

(define (make-skybox cube-map)
  (let* ((index (u32vector 0 3 2 0 2 1
                           4 7 6 4 6 5
                           8 11 10 8 10 9
                           12 15 14 12 14 13
                           16 19 18 16 18 17
                           20 23 22 20 22 21))
         (verts (f32vector -1.0 -1.0 -1.0 ; bottom
                           1.0 -1.0 -1.0
                           1.0 -1.0 1.0
                           -1.0 -1.0 1.0
                           -1.0 1.0 -1.0 ; top
                           1.0 1.0 -1.0
                           1.0 1.0 1.0
                           -1.0 1.0 1.0
                           -1.0 -1.0 -1.0 ; left
                           -1.0 1.0 -1.0
                           -1.0 1.0 1.0
                           -1.0 -1.0 1.0
                           1.0 -1.0 -1.0 ; right
                           1.0 1.0 -1.0
                           1.0 1.0 1.0
                           1.0 -1.0 1.0
                           -1.0 -1.0 1.0 ; front
                           1.0 -1.0 1.0
                           1.0 1.0 1.0
                           -1.0 1.0 1.0
                           -1.0 -1.0 -1.0 ; back
                           1.0 -1.0 -1.0
                           1.0 1.0 -1.0
                           -1.0 1.0 -1.0))
         (index-buffer (make-buffer index #:target 'index))
         (vertex-buffer (make-buffer verts))
         (indices (make-vertex-attribute #:buffer index-buffer
                                         #:type 'scalar
                                         #:component-type 'unsigned-int))
         (positions (make-vertex-attribute #:buffer vertex-buffer
                                           #:type 'vec3
                                           #:component-type 'float))
         (va (make-vertex-array #:indices indices
                                #:attributes `((0 . ,positions)))))
    (%make-skybox cube-map va)))

(define %default-skybox-face
  (bytevector->pixbuf (u32vector #xff202020 #xff202020 #xff202020 #xff202020)
                      2 2))

;; A default, dark gray skybox.
(define %default-skybox
  (delay
    (make-skybox (make-cube-map #:right %default-skybox-face
                                #:left %default-skybox-face
                                #:top %default-skybox-face
                                #:bottom %default-skybox-face
                                #:front %default-skybox-face
                                #:back %default-skybox-face))))

(define (default-skybox)
  (force %default-skybox))

(define %skybox-shader
  (delay (load-shader (scope-datadir "shaders/skybox-vert.glsl")
                      (scope-datadir "shaders/skybox-frag.glsl"))))

(define (skybox-shader)
  (force %skybox-shader))

(define (draw-skybox skybox view)
  (with-graphics-state ((g:texture-0 (skybox-cube-map skybox)))
    (shader-apply (skybox-shader) (skybox-vertex-array skybox)
                  #:view view
                  #:projection (current-projection))))
