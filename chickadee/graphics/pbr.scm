;;; Chickadee Game Toolkit
;;; Copyright © 2019, 2021 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; Physically based lighting model.
;;
;;; Code:

(define-module (chickadee graphics pbr)
  #:use-module (chickadee config)
  #:use-module (chickadee math vector)
  #:use-module (chickadee graphics blend)
  #:use-module (chickadee graphics buffer)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics depth)
  #:use-module (chickadee graphics engine)
  #:use-module (chickadee graphics mesh)
  #:use-module (chickadee graphics polygon)
  #:use-module (chickadee graphics shader)
  #:use-module (chickadee graphics stencil)
  #:use-module (chickadee graphics texture)
  #:use-module (srfi srfi-9)
  #:export (make-pbr-properties
            pbr-properties?
            pbr-properties-base-color-factor
            pbr-properties-base-color-texcoord
            pbr-properties-metallic-factor
            pbr-properties-roughness-factor
            pbr-properties-metallic-roughness-texcoord
            pbr-properties-normal-texcoord
            pbr-properties-occlusion-texcoord
            pbr-properties-emissive-factor
            pbr-properties-emissive-texcoord
            pbr-properties-alpha-mode
            pbr-properties-alpha-cutoff
            pbr-shader
            make-pbr-material))

(define-shader-type <pbr-properties>
  make-pbr-properties
  pbr-properties?
  (float-vec3 base-color-factor pbr-properties-base-color-factor)
  (int base-color-texcoord pbr-properties-base-color-texcoord)
  (float metallic-factor pbr-properties-metallic-factor)
  (float roughness-factor pbr-properties-roughness-factor)
  (int metallic-roughness-texcoord pbr-properties-metallic-roughness-texcoord)
  (int normal-texcoord pbr-properties-normal-texcoord)
  (int occlusion-texcoord pbr-properties-occlusion-texcoord)
  (float-vec3 emissive-factor pbr-properties-emissive-factor)
  (int emissive-texcoord pbr-properties-emissive-texcoord)
  (int alpha-mode pbr-properties-alpha-mode)
  (float alpha-cutoff pbr-properties-alpha-cutoff))

(define %pbr-shader
  (delay (load-shader (scope-datadir "shaders/pbr-vert.glsl")
                      (scope-datadir "shaders/pbr-frag.glsl"))))

(define (pbr-shader)
  (force %pbr-shader))

(define* (make-pbr-material #:key (name "anonymous")
                            (blend-mode blend:replace)
                            (polygon-mode fill-polygon-mode)
                            (cull-face-mode back-cull-face-mode)
                            (depth-test basic-depth-test)
                            (stencil-test default-stencil-test)
                            multisample?
                            (base-color-factor (vec3 1.0 1.0 1.0))
                            (base-color-texcoord 0)
                            (metallic-factor 1.0)
                            (roughness-factor 1.0)
                            (metallic-roughness-texcoord 0)
                            (normal-texcoord 0)
                            (occlusion-texcoord 0)
                            (emissive-factor (vec3 1.0 1.0 1.0))
                            (emissive-texcoord 0)
                            (alpha-mode 'opaque)
                            (alpha-cutoff 0.5)
                            (base-color-texture (white-texture))
                            (metallic-roughness-texture (white-texture))
                            (normal-texture (flat-texture))
                            (occlusion-texture (white-texture))
                            (emissive-texture (black-texture)))
  (let ((properties (make-pbr-properties
                     #:base-color-factor base-color-factor
                     #:base-color-texcoord base-color-texcoord
                     #:metallic-factor metallic-factor
                     #:roughness-factor roughness-factor
                     #:metallic-roughness-texcoord metallic-roughness-texcoord
                     #:normal-texcoord normal-texcoord
                     #:occlusion-texcoord occlusion-texcoord
                     #:emissive-factor emissive-factor
                     #:emissive-texcoord emissive-texcoord
                     #:alpha-mode (case alpha-mode
                                    ((opaque) 0)
                                    ((mask) 1)
                                    ((blend) 2)
                                    (else
                                     (error "unsupport PBR alpha mode:"
                                            alpha-mode)))
                     #:alpha-cutoff 0.5)))
    (make-material #:name name
                   #:shader (pbr-shader)
                   #:texture-0 base-color-texture
                   #:texture-1 metallic-roughness-texture
                   #:texture-2 normal-texture
                   #:texture-3 occlusion-texture
                   #:texture-4 emissive-texture
                   #:properties properties)))

;; TODO: Handle vertex colors
;; (buffer-view?
;;  (assv-ref vattrs
;;            (attribute-location
;;             (hash-ref sattrs "color0"))))
