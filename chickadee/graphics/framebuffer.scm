;;; Chickadee Game Toolkit
;;; Copyright © 2017, 2021 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; Render to texture.
;;
;;; Code:

(define-module (chickadee graphics framebuffer)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:use-module (system foreign)
  #:use-module (gl)
  #:use-module (gl enums)
  #:use-module (chickadee math matrix)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics engine)
  #:use-module (chickadee graphics gl)
  #:use-module (chickadee graphics pixbuf)
  #:use-module ((chickadee graphics texture) #:select (make-texture null-texture))
  #:use-module (chickadee graphics viewport)
  #:export (make-framebuffer
            framebuffer?
            framebuffer-texture
            framebuffer-viewport
            framebuffer-projection
            null-framebuffer
            g:framebuffer
            current-framebuffer
            with-framebuffer))

(define (generate-framebuffer)
  "Generate a new OpenGL framebuffer object."
  (let ((bv (u32vector 1)))
    (gl-gen-framebuffers 1 (bytevector->pointer bv))
    (u32vector-ref bv 0)))

(define (generate-renderbuffer)
  "Generate a new OpenGL renderbuffer object."
  (let ((bv (u32vector 1)))
    (gl-gen-renderbuffers 1 (bytevector->pointer bv))
    (u32vector-ref bv 0)))

(define-record-type <framebuffer>
  (%make-framebuffer id renderbuffer-id texture viewport projection)
  framebuffer?
  (id framebuffer-id)
  (renderbuffer-id framebuffer-renderbuffer-id)
  (texture framebuffer-texture)
  (viewport framebuffer-viewport)
  (projection framebuffer-projection))

(define null-framebuffer
  (%make-framebuffer 0 0 null-texture null-viewport (make-identity-matrix4)))

(define (free-framebuffer framebuffer)
  (gl-delete-renderbuffers 1
                           (bytevector->pointer
                            (u32vector
                             (framebuffer-renderbuffer-id framebuffer))))
  (gl-delete-framebuffers 1
                          (bytevector->pointer
                            (u32vector
                             (framebuffer-id framebuffer)))))

(define (apply-framebuffer framebuffer)
  (gl-bind-framebuffer (version-3-0 framebuffer)
                       (framebuffer-id framebuffer)))

(define (bind-framebuffer framebuffer)
  (gl-bind-framebuffer (version-3-0 framebuffer)
                       (framebuffer-id framebuffer)))

(define-graphics-finalizer framebuffer-finalizer
  #:predicate framebuffer?
  #:free free-framebuffer)

(define-graphics-state g:framebuffer
  current-framebuffer
  #:default null-framebuffer
  #:bind bind-framebuffer)

(define %clear-color (transparency 0.0))
(define %draw-buffers (bytevector->pointer (u32vector (version-3-0 color-attachment0))))

(define* (make-framebuffer width height #:key (min-filter 'linear) (mag-filter 'linear)
                           (wrap-s 'repeat) (wrap-t 'repeat))
  "Create a new framebuffer that renders to a texture with
dimensions WIDTH x HEIGHT."
  (assert-current-graphics-engine)
  (let* ((framebuffer-id (generate-framebuffer))
         (renderbuffer-id (generate-renderbuffer))
         (texture (make-texture width height
                                #:min-filter min-filter
                                #:mag-filter mag-filter
                                #:wrap-s wrap-s
                                #:wrap-t wrap-t))
         ;; It is convenient to make a default viewport and
         ;; projection matrix for the framebuffer so that the
         ;; rendering engine can set it whenever it changes to
         ;; this framebuffer, saving users the trouble of having
         ;; to this tedious task themselves.
         (viewport (make-viewport 0 0 width height #:clear-color %clear-color))
         (projection (orthographic-projection 0 width height 0 0 1))
         (framebuffer (%make-framebuffer framebuffer-id
                                         renderbuffer-id
                                         texture
                                         viewport
                                         projection)))
    (graphics-engine-guard! framebuffer)
    (with-graphics-state! ((g:framebuffer framebuffer))
      ;; Setup depth buffer.
      (gl-bind-renderbuffer (version-3-0 renderbuffer)
                            renderbuffer-id)
      (gl-renderbuffer-storage (version-3-0 renderbuffer)
                               (arb-framebuffer-object depth24-stencil8)
                               width
                               height)
      (gl-bind-renderbuffer (version-3-0 renderbuffer) 0)
      (gl-framebuffer-renderbuffer (version-3-0 framebuffer)
                                   (arb-framebuffer-object depth-stencil-attachment)
                                   (version-3-0 renderbuffer)
                                   renderbuffer-id)
      ;; Setup framebuffer.
      (gl-framebuffer-texture-2d (version-3-0 framebuffer)
                                 (version-3-0 color-attachment0)
                                 (texture-target texture-2d)
                                 ((@@ (chickadee graphics texture) texture-id)
                                  texture)
                                 0)
      (gl-draw-buffers 1 %draw-buffers))
    ;; Check for errors.
    (unless (= (gl-check-framebuffer-status (version-3-0 framebuffer))
               (version-3-0 framebuffer-complete))
      (error "Framebuffer cannot be created"))
    framebuffer))

(define-syntax-rule (with-framebuffer framebuffer body ...)
  ;; As a convenience, initialize the viewport and projection matrix
  ;; as well so that the user doesn't have to explicitly make a
  ;; viewport and/or projection matrix unless they actually want to do
  ;; fancy viewport manipulations.
  (with-graphics-state! ((g:framebuffer framebuffer)
                         (g:viewport (framebuffer-viewport framebuffer)))
    (clear-viewport)
    (with-projection (framebuffer-projection framebuffer)
      body ...)))
