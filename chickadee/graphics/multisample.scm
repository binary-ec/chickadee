;;; Chickadee Game Toolkit
;;; Copyright © 2021 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; Multisampling.
;;
;;; Code:

(define-module (chickadee graphics multisample)
  #:use-module (chickadee graphics engine)
  #:use-module (gl)
  #:export (g:multisample?
            current-multisample))

(define (bind-multisample multisample?)
  (if multisample?
      (gl-enable (version-1-3 multisample))
      (gl-disable (version-1-3 multisample))))

(define-graphics-state g:multisample?
  current-multisample
  #:default #f
  #:bind bind-multisample)
