;;; Chickadee Game Toolkit
;;; Copyright © 2019, 2021 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;
;; Simple forward rendered Phong lighting model.
;;
;;; Code:

(define-module (chickadee graphics phong)
  #:use-module (chickadee config)
  #:use-module (chickadee math vector)
  #:use-module (chickadee graphics blend)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics depth)
  #:use-module (chickadee graphics engine)
  #:use-module (chickadee graphics light)
  #:use-module (chickadee graphics mesh)
  #:use-module (chickadee graphics polygon)
  #:use-module (chickadee graphics shader)
  #:use-module (chickadee graphics stencil)
  #:use-module (chickadee graphics texture)
  #:use-module (srfi srfi-9)
  #:export (make-phong-properties
            phong-properties?
            phong-properties-ambient
            phong-properties-diffuse
            phong-properties-specular
            phong-properties-shininess
            phong-shader
            make-phong-material))

(define-shader-type <phong-properties>
  make-phong-properties
  phong-properties?
  (float-vec3 ambient phong-properties-ambient)
  (float-vec3 diffuse phong-properties-diffuse)
  (float-vec3 specular phong-properties-specular)
  (float shininess phong-properties-shininess))

(define %phong-shader
  (delay (load-shader (scope-datadir "shaders/phong-vert.glsl")
                      (scope-datadir "shaders/phong-frag.glsl"))))

(define (phong-shader)
  (force %phong-shader))

(define* (make-phong-material #:key (name "anonymous")
                              (blend-mode blend:replace)
                              (polygon-mode fill-polygon-mode)
                              (cull-face-mode back-cull-face-mode)
                              (depth-test basic-depth-test)
                              (stencil-test default-stencil-test)
                              multisample?
                              (ambient-factor (vec3 1.0 1.0 1.0))
                              (diffuse-factor (vec3 1.0 1.0 1.0))
                              (specular-factor (vec3 1.0 1.0 1.0))
                              (shininess 32.0)
                              (ambient-texture (white-texture))
                              (diffuse-texture (white-texture))
                              (specular-texture (white-texture))
                              (normal-texture (flat-texture)))
  (make-material #:name name
                 #:shader (phong-shader)
                 #:texture-0 ambient-texture
                 #:texture-1 diffuse-texture
                 #:texture-2 specular-texture
                 #:texture-3 normal-texture
                 #:properties (make-phong-properties #:ambient ambient-factor
                                                     #:diffuse diffuse-factor
                                                     #:specular specular-factor
                                                     #:shininess shininess)))
