;;; Chickadee Game Toolkit
;;; Copyright © 2017, 2020 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (chickadee scripting)
  #:use-module (chickadee game-loop)
  #:use-module (chickadee math)
  #:use-module (chickadee math easings)
  #:use-module (chickadee scripting agenda)
  #:use-module (chickadee scripting channel)
  #:use-module (chickadee scripting script)
  #:export (forever
            repeat
            sleep
            wait-until
            tween)
  #:replace (sleep))

;; Export public bindings from other modules.
(eval-when (eval load compile)
  (begin
    (define %public-modules
      '(agenda channel script))
    (for-each (let ((i (module-public-interface (current-module))))
                (lambda (m)
                  (module-use! i (resolve-interface
                                  `(chickadee scripting ,m)))))
              %public-modules)))

(define-syntax-rule (forever body ...)
  "Evaluate BODY in an endless loop."
  (while #t body ...))

(define-syntax-rule (repeat n body ...)
  "Evaluate BODY N times."
  (let loop ((i 0))
    (when (< i n)
      body ...
      (loop (+ i 1)))))

(define (sleep duration)
  "Wait DURATION before resuming the current script."
  ;; Capture the current agenda before suspending the script so that
  ;; we schedule the continuation in the right place.
  (let ((agenda (current-agenda)))
    (yield
     (lambda (cont)
       (with-agenda agenda
         (schedule-after duration cont))))))

(define-syntax-rule (wait-until condition)
  "Pause current script until CONDITION has been met."
  ;; Don't pause if the condition is already met.
  (unless condition
    (let ((agenda (current-agenda)))
      (yield
       (lambda (cont)
         (with-agenda agenda
           (call-when (lambda () condition)
                      cont)))))))

(define* (tween duration start end proc #:key
                (step (current-timestep))
                (ease smoothstep)
                (interpolate lerp))
  "Transition a value from START to END over DURATION, sending each
succesive value to PROC.  STEP controls the amount of time between
each update of the animation.

The EASE procedure controls the rate at which the animation advances.
The smoothstep easing function is used by default.

The INTERPOLATE procedure computes the values in between START and
END.  By default, linear interpolation is used."
  (let loop ((t 0))
    (if (>= t duration)
        (proc end)
        (let ((alpha (ease (/ t duration))))
          (proc (interpolate start end alpha))
          (sleep step)
          (loop (+ t step))))))
